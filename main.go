package main

import (
	"bufio"
	"flag"
	"fmt"
	"io"
	"log"
	"os"
)

var allp = flag.Bool("all", false, "Show all counts (including zero counts)")

func main() {
	flag.Parse()

	count := make([]int, 256)

	args := flag.Args()

	for _, arg := range args {
		var r io.ReadCloser
		var err error

		if arg == "-" {
			r = os.Stdin
		} else {
			r, err = os.Open(arg)
			if err != nil {
				log.Fatal(err)
			}
			defer r.Close()
		}

		Count(r, count)
	}

	for i, c := range count {
		if c > 0 || *allp {
			fmt.Printf("%d %d\n", i, c)
		}
	}
}

func Count(r io.Reader, count []int) {
	buf := make([]byte, 1)
	in := bufio.NewReader(r)

	for {
		n, _ := in.Read(buf)
		if n == 0 {
			break
		}
		count[buf[0]] += 1
	}
}
