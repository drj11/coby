# coby

`coby` — COunt BYtes

## Usage

    coby [-all] [file ...]

Reads files (use "-" to read `stdin`) and
outputs a report of the count of each byte value found.
Normally only byte values that have a non-zero count are
reported, using the `-all` flag means that all 256 byte counts
are reported.

Byte counts are reported in order of the byte value.

## Example

    echo foo | coby

Outputs:

    10 1
    102 1
    111 2

10 is the ASCII code for newline (`echo` outputs a newline),
102 is the ASCII code for `f`,
111 is the ASCII code for `o` (of which there are 2).

## Discussion

This tool only works with bytes, by intention.
So if you have Unicode in your input, and you are use UTF-8
(most environments in 2020), then this tool will count the
byte in the UTF-8 encoding:

    printf · | coby

Outputs

    183 1
    194 1

## END
